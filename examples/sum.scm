;; print sum of two numbers

(define (sum a b)
  (display (+ a b))
  (newline)
  )

;; test casese
(sum 5 6)
(sum 9 60)
(sum 6060 909)
